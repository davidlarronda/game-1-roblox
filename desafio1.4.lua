--crio a variável local bola

local bola = script.Parent

--crio a variável local i
local i = 1

--[[ agora espero 10 seg(é importante usar o comando wait() para não gerar loops infinitos ]]

wait(10)

repeat

	bola.BrickColor = BrickColor.Random() -- a propriedade Random() faz os valores serem aleatórios

	i = i + 1

	wait(2)


until i == 10 -- até quando vou fazer?
